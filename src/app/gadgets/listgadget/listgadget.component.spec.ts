import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListgadgetComponent } from './listgadget.component';

describe('ListgadgetComponent', () => {
  let component: ListgadgetComponent;
  let fixture: ComponentFixture<ListgadgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListgadgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListgadgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
