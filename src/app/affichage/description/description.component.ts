import { Component, Input, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {
@Input() data:any;
@Output() changeComponent=new EventEmitter<boolean>();
Changevalue=true;
url="/editor/disps/";

  constructor() { }

  ngOnInit(): void {
  }

  reload(value:boolean){

    this.changeComponent.emit(value);
    this.data.show=false;


  }
}
