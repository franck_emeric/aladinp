import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinModelsComponent } from './aladin-models.component';

describe('AladinModelsComponent', () => {
  let component: AladinModelsComponent;
  let fixture: ComponentFixture<AladinModelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinModelsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinModelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
