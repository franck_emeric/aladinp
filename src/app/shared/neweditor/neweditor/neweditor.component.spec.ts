import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NeweditorComponent } from './neweditor.component';

describe('NeweditorComponent', () => {
  let component: NeweditorComponent;
  let fixture: ComponentFixture<NeweditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NeweditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NeweditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
